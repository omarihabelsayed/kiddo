﻿using Enums;
using UnityEngine;


public class Block : ScriptableObject
{
    public Category category;
    public bool IsActive = false;

}

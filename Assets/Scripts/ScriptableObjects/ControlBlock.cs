﻿using Enums;
using UnityEngine;


[CreateAssetMenu(fileName = "New Control Block", menuName = "Blocks/Control Block", order = 2)]
public class ControlBlock : Block
{
    Category category = Category.PlayerControl;

    [SerializeField]
    public ControlFunctions function;
}

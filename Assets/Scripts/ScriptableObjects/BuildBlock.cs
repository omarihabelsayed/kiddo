﻿using Enums;
using UnityEngine;


[CreateAssetMenu(fileName = "New Build Block", menuName = "Blocks/Build Block", order = 3)]
public class BuildBlock : Block
{
    Category category = Category.Builder;

    [SerializeField]
    public BuildFunctions function;
}

﻿using Enums;
using UnityEngine;

[CreateAssetMenu(fileName = "New Camera Block", menuName = "Blocks/Camera Block", order = 1)]
public class CameraBlock : Block
{
    Category category = Category.CameraControl;

    [SerializeField]
    public CameraFunctions function;
}

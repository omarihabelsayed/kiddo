﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardManager : MonoBehaviour
{
    [SerializeField]
    Material[] SkyboxMaterials;

    [SerializeField]
    GameObject[] l_go_scenes;

    int MatIndex = 0;
    int ScenesIndex = 0;

    // Start is called before the first frame update


    public void ChangeSkyboxForward()
    {
        MatIndex++;
        MatIndex %= SkyboxMaterials.Length;
        RenderSettings.skybox = SkyboxMaterials[MatIndex];
    }
    public void ChangeSkyboxBackward()
    {
        MatIndex--;
        if (MatIndex == -1)
            MatIndex = SkyboxMaterials.Length - 1;
        RenderSettings.skybox = SkyboxMaterials[MatIndex];
    }

    public void ChangeSceneFowrard()
    {
        l_go_scenes[ScenesIndex].SetActive(false);
        ScenesIndex++;
        ScenesIndex %= l_go_scenes.Length;
        l_go_scenes[ScenesIndex].SetActive(true);

    }

    public void ChangeSceneBackward()
    {
        l_go_scenes[ScenesIndex].SetActive(false);
        ScenesIndex--;
        if (ScenesIndex == 0)
            ScenesIndex = l_go_scenes.Length - 1;
        l_go_scenes[ScenesIndex].SetActive(true);

    }
}

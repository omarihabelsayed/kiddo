﻿using Enums;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    #region Singleton
   
    #endregion

    #region Variables

    #region Booleans

    #endregion

    #region Vectors And Transforms

    private Vector3 v_StartPosition;
    private Transform t_StartParent;
    #endregion

    #region Integers And Floats
    #endregion

    #region Strings And Enums
    #endregion

    #region Lists
    #endregion

    #region Public GameObjects

    public static GameObject go_ItemDragged;
    #endregion

    #region Private GameObjects

    #endregion

    #region UIElements
    #endregion

    #region Others
    #endregion

    #endregion


    #region Main Methods
   
    #endregion

    #region Methods
    #endregion

    #region Collisons And Triggers
    #endregion

    #region Coroutines
    #endregion

    public void OnBeginDrag(PointerEventData eventData)
    {
        go_ItemDragged = gameObject;
        v_StartPosition = transform.position;
        t_StartParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        go_ItemDragged = null;
        Block block = transform.gameObject.GetComponent<BlockController>().block;
        if (transform.gameObject.GetComponent<BlockController>().block.category == Category.Builder)
        {
            BuildBlock newBlock = (BuildBlock)block;
            RaycastHit hit;
            Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(r, out hit, 1000))
            {
                
               if(newBlock.function == BuildFunctions.Cube)
               {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.AddComponent<Rigidbody>();
                    cube.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    cube.transform.position = hit.point + new Vector3(0, 0.5f, 0);
               }
                if (newBlock.function == BuildFunctions.Sphere)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    cube.AddComponent<Rigidbody>();
                    cube.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    cube.transform.position = hit.point + new Vector3(0, 0.5f, 0);
                }
                if (newBlock.function == BuildFunctions.Cylinder)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                    cube.AddComponent<Rigidbody>();
                    cube.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    cube.transform.position = hit.point + new Vector3(0, 0.5f, 0);
                }
            }
           
        }
      
        
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (transform.parent == t_StartParent)
        {
            transform.position = v_StartPosition;
        }
        else
        {
            Debug.Log("Parent is: " + transform.parent.name);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class PanelExecutor : MonoBehaviour
{
    #region Singleton
    public static PanelExecutor _ControlPanelInstance;
    #endregion

    #region Variables

    #region Booleans

    #endregion

    #region Vectors And Transforms

    #endregion

    #region Integers And Floats
    #endregion

    #region Strings And Enums
    #endregion

    #region Lists
    [SerializeField]
    GameObject[] l_go_WhiteBoardBlocks;
    #endregion

    #region Public GameObjects
    [SerializeField]
    GameObject go_MainPlayer;
    #endregion

    #region Private GameObjects
    #endregion

    #region UIElements
    #endregion

    #region Others
    
    #endregion

    #endregion


    private void Awake()
    {
        _ControlPanelInstance = this;
    }

    #region Methods
    public void GetBlockCategory(GameObject go_block)
    {
        Block block = go_block.GetComponent<BlockController>().block;
        if (block.category == Category.PlayerControl)
        {
            GetControlFunction((ControlBlock)block);
        }

        else if (block.category == Category.CameraControl)
        {
            GetCameraFunction((CameraBlock)block);
        }

        else if (block.category == Category.Builder)
        {

        }
    }

    public void RemoveBlockCategory(GameObject go_block)
    {
        Block block = go_block.GetComponent<BlockController>().block;
        if (block.category == Category.PlayerControl)
        {
            RemoveControlFunction((ControlBlock)block);
        }

        else if (block.category == Category.CameraControl)
        {
            RemoveCameraFunction((CameraBlock)block);
        }

        else if (block.category == Category.Builder)
        {

        }
    }


    private void GetControlFunction(ControlBlock controlBlock)
    {
        switch (controlBlock.function)
        {
            case ControlFunctions.MoveH:
                go_MainPlayer.AddComponent<MoveHorizontal>();
                Debug.Log("I WILL EXECUTE MOVE H");
                break;
            case ControlFunctions.MoveV:
                go_MainPlayer.AddComponent<MoveVertical>();
                Debug.Log("I WILL EXECUTE MOVE V");
                break;
            case ControlFunctions.Jump:
                go_MainPlayer.AddComponent<Jump>();
                Debug.Log("I WILL EXECUTE JUMP");
                break;
            case ControlFunctions.Shoot:
                go_MainPlayer.AddComponent<Shoot>();
                Debug.Log("I WILL EXECUTE SHOOT");
                break;
        }
    }
    private void RemoveControlFunction(ControlBlock controlBlock)
    {
        switch (controlBlock.function)
        {
            case ControlFunctions.MoveH:
                Destroy(go_MainPlayer.GetComponent<MoveHorizontal>());
                Debug.Log("I WILL EXECUTE MOVE H");
                break;
            case ControlFunctions.MoveV:
                Destroy(go_MainPlayer.GetComponent<MoveVertical>());
                Debug.Log("I WILL EXECUTE MOVE V");
                break;
            case ControlFunctions.Jump:
                Destroy(go_MainPlayer.GetComponent<Jump>());
                Debug.Log("I WILL EXECUTE JUMP");
                break;
            case ControlFunctions.Shoot:
                Destroy(go_MainPlayer.GetComponent<Shoot>());
                Debug.Log("I WILL EXECUTE SHOOT");
                break;
        }
    }

    private void GetCameraFunction(CameraBlock cameraBlock)
    {
        Vector3 newCamPos = new Vector3(0, 0, 0);
        Quaternion newCamRot = new Quaternion(0, 0, 0, 0);
        Vector3 rotVector = new Vector3(0, 0, 0);
        switch (cameraBlock.function)
        {
            case CameraFunctions.FirstPerson:
                newCamPos = new Vector3(0, 1, -8);
                rotVector = new Vector3(0, 0, 0);
                newCamRot.eulerAngles = rotVector;
                Camera.main.transform.position = newCamPos;
                Camera.main.transform.rotation = newCamRot;
                break;
            case CameraFunctions.ThirdPerson:
                newCamPos = new Vector3(0, 1.85f, -9.5f);
                rotVector = new Vector3(15, 0, 0);
                newCamRot.eulerAngles = rotVector;
                Camera.main.transform.position = newCamPos;
                Camera.main.transform.rotation = newCamRot;
                break;
            case CameraFunctions.TopDown:
                newCamPos = new Vector3(2.5f, 2.85f, -8.6f);
                rotVector = new Vector3(43, -63.5f, 0);
                newCamRot.eulerAngles = rotVector;
                Camera.main.transform.position = newCamPos;
                Camera.main.transform.rotation = newCamRot;
                break;
            case CameraFunctions.TwoD:
                newCamPos = new Vector3(-4.5f, 1.5f, -5.9f);
                rotVector = new Vector3(1, 78.5f, 0);
                newCamRot.eulerAngles = rotVector;
                Camera.main.transform.position = newCamPos;
                Camera.main.transform.rotation = newCamRot;
                break;
        }

    }
    private void RemoveCameraFunction(CameraBlock cameraBlock)
    {

        Vector3 newCamPos = new Vector3(0, 1, -10);
        Quaternion newCamRot = new Quaternion(0, 0, 0, 0);
        Vector3 rotVector = new Vector3(0, 0, 0);
        newCamRot.eulerAngles = rotVector;
        Camera.main.transform.position = newCamPos;
        Camera.main.transform.rotation = newCamRot;
    }
    #endregion

    #region Collisons And Triggers
    #endregion

    #region Coroutines
    #endregion
}

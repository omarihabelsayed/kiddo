﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
   
    Pool BulletsPool;
    
    // Start is called before the first frame update
    void Start()
    {
        BulletsPool = Resources.Load("PlayerBulletsPool") as Pool;
        BulletsPool.InitializePool();
    }

    // Update is called once per frame
    void Update()
    {
        Fire();
    }

    void Fire()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            BulletsPool.SpawnFromPool(transform, 1000);
            
        }
    }


   
}

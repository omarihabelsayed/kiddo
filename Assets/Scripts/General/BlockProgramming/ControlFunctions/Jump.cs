﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    Rigidbody rb_PlayerRigidBody;
    private void Start()
    {
        rb_PlayerRigidBody = GetComponent<Rigidbody>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            JumpUp();
        }
    }

    void JumpUp()
    {
        rb_PlayerRigidBody.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
    }
}

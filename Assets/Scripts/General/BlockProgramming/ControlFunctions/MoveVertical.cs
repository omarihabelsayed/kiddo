﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveVertical : MonoBehaviour
{
    
    // Update is called once per frame
    void Update()
    {
        Move();
        
    }


    void Move()
    {
        float translationZ = Input.GetAxis("Vertical") * 2.5f;
        //float translationX = Input.GetAxis("Horizontal") * 5;

        translationZ *= Time.deltaTime;
        //translationX *= Time.deltaTime;

        transform.Translate(Vector3.forward * translationZ, Space.Self);
        //transform.Translate(Vector3.right * translationX, Space.Self);

    }

    

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveHorizontal : MonoBehaviour
{

    Rigidbody rb_PlayerRigidBody;
    private void Start()
    {
        rb_PlayerRigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Turning();
    }


    void Move()
    {
        //float translationZ = Input.GetAxis("Vertical") * 5;
        float translationX = Input.GetAxis("Horizontal") * 2.5f;

        //translationZ *= Time.deltaTime;
        translationX *= Time.deltaTime;

        //transform.Translate(Vector3.forward * translationZ, Space.Self);
        transform.Translate(Vector3.right * translationX, Space.Self);

    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, 100, 1))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            rb_PlayerRigidBody.MoveRotation(newRotation);
        }
    }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SlotsHandler : MonoBehaviour, IDropHandler
{
    #region Singleton
    PanelExecutor controlPanelInstance;
    #endregion

    #region Variables

    #region Booleans

    #endregion

    #region Vectors And Transforms

    #endregion

    #region Integers And Floats
    #endregion

    #region Strings And Enums
    #endregion

    #region Lists
    #endregion

    #region Public GameObjects
    #endregion

    #region Private GameObjects
    #endregion

    #region UIElements
    #endregion

    #region Others
    #endregion

    #endregion


    #region Main Methods
    private void Start()
    {
        controlPanelInstance = PanelExecutor._ControlPanelInstance;
    }
    #endregion

    #region Methods

    public Transform item()
    {
        if (transform.childCount > 0)
        {
            return transform.GetChild(0);
        }

        return null;
    }
    public void OnDrop(PointerEventData eventData)
    {
        if (!item())
        {
            DragHandler.go_ItemDragged.transform.SetParent(transform);
            DragHandler.go_ItemDragged.transform.position = transform.position;
            if(transform.CompareTag("ExecutablePanel"))
                controlPanelInstance.GetBlockCategory(DragHandler.go_ItemDragged);
            else
            {
                controlPanelInstance.RemoveBlockCategory(DragHandler.go_ItemDragged);
            }
            
        }
    }
    
    
    #endregion

    #region Collisons And Triggers
    #endregion

    #region Coroutines
    #endregion

   
}

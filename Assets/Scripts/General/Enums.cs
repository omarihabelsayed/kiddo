﻿
namespace Enums
{
    public enum Category
    {
        CameraControl,
        PlayerControl,
        Builder
    }

    public enum ControlFunctions
    {
        MoveH,
        MoveV,
        Jump,
        Shoot
    }


    public enum CameraFunctions
    {
        FirstPerson,
        ThirdPerson,
        TopDown,
        TwoD
    }

    public enum BuildFunctions
    {
        Cube,
        Sphere,
        Cylinder
    }
}

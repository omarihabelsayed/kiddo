﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager1 : MonoBehaviour
{
    [SerializeField]
    GameObject go_Green;
    [SerializeField]
    GameObject go_Blue;
    [SerializeField]
    GameObject go_Orange;
    [SerializeField]
    GameObject go_Yellow;

    bool IsOff;
    // Start is called before the first frame update
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EscapeButton();
        }
    }

    public void SwitchToYellow()
   {
        go_Yellow.SetActive(true);
        go_Blue.SetActive(false);
        go_Green.SetActive(false);
        go_Orange.SetActive(false);
   }
    public void SwitchToBlue()
    {
        go_Yellow.SetActive(false);
        go_Blue.SetActive(true);
        go_Green.SetActive(false);
        go_Orange.SetActive(false);
    }
    public void SwitchToGreen()
    {
        go_Yellow.SetActive(false);
        go_Blue.SetActive(false);
        go_Green.SetActive(true);
        go_Orange.SetActive(false);
    }
    public void SwitchToOrange()
    {
        go_Yellow.SetActive(false);
        go_Blue.SetActive(false);
        go_Green.SetActive(false);
        go_Orange.SetActive(true);
    }
    public void EscapeButton()
    {
        if (IsOff)
        {
            go_Yellow.SetActive(false);
            go_Blue.SetActive(false);
            go_Green.SetActive(true);
            go_Orange.SetActive(false);
            IsOff = !IsOff;
        }
        else
        {
            go_Yellow.SetActive(false);
            go_Blue.SetActive(false);
            go_Green.SetActive(false);
            go_Orange.SetActive(false);
            IsOff = !IsOff;
        }
       
    }

}
